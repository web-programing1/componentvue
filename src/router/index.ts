import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/my-file",
      name: "myfile",
      component: () => import("../views/MyFileView.vue"),
    },
    {
      path: "/share-with-me",
      name: "share-with-me",
      component: () => import("../views/ShareWithMe.vue"),
    },
    {
      path: "/starred",
      name: "starred",
      component: () => import("../views/StarredView.vue"),
    },
    {
      path: "/my-profile",
      name: "my-profile",
      component: () => import("../views/MyProfile.vue"),
    },
  ],
});

export default router;
